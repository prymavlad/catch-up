import { timeSlots } from './constants.js';
import { lessons } from './constants.js';

const submitBtn = document.querySelector('#booking_lesson');
const timeInputs = document.querySelectorAll('input[name="time"]');
const lessonInputs = document.querySelectorAll('input[name="type"]');
const logoutBtn = document.querySelector('#logoutBtn');
const bookingLesson = [];

logoutBtn.addEventListener('click', () => {
    localStorage.removeItem('login');
});

submitBtn.addEventListener('click', function (e) {

    e.preventDefault();

    const checkedTime = checked(timeInputs);
    const checkedLesson = checked(lessonInputs);
    const login = localStorage.getItem('login');
    const loginObj = JSON.parse(login);
    let tomorrow = false;

    if(checkedTime === 'time_04' || checkedTime === 'time_05' || checkedTime === 'time_06') {
        tomorrow = true;
    }

    console.log('name', loginObj.name);
    console.log('time:', timeSlots[checkedTime]);
    console.log('tomorrow:', tomorrow);
    console.log('title:', lessons[checkedLesson].title);
    console.log('duration:', lessons[checkedLesson].duration);

    bookingLesson.push({
        name: loginObj.name,
        time: timeSlots[checkedTime],
        tomorrow: tomorrow,
        title: lessons[checkedLesson].title,
        duration: lessons[checkedLesson].duration,
    });

    localStorage.setItem('lesson', JSON.stringify(bookingLesson));

});


function checked(inputs) {
    let id = null;

    inputs.forEach((el) => {
        if(el.checked) {
            id = el.id;
        }
    });

    return id;
}