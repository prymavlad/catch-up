import { saveToStorage } from './login';

// general
const teacher = {};
const student = [];

const typeInputs = document.querySelectorAll('input[name="user-type"]');

// registration
const regBtn = document.querySelector('#regBtn');
const loginBlock = document.querySelector('#loginBlock');
const createAccount = document.querySelector('#createAccount');

// step 1
const toStep2Btn = document.querySelector('#toStep2Btn');
const step1Block = document.querySelector('#step1Block');
const toLoginSvg = document.querySelector('#toLoginSvg');
const from1to2 = document.querySelector('#from1to2');

// step 2
const regBlock = document.querySelector('#regBlock');
const from3to2Svg = document.querySelector('#from3to2Svg');
const from2to1 = document.querySelector('#from2to1');

regBtn.addEventListener('click', function() {
    loginBlock.style.display = 'none';
    step1Block.style.display = 'flex';
});

from1to2.addEventListener('click', function() {
    step1Block.style.display = 'none';
    regBlock.style.display = 'flex';
});

toStep2Btn.addEventListener('click', function() {
    step1Block.style.display = 'none';
    regBlock.style.display = 'flex';
});

from2to1.addEventListener('click', function() {
    regBlock.style.display = 'none';
    step1Block.style.display = 'flex';
});

toLoginSvg.addEventListener('click', function() {
    step1Block.style.display = 'none';
    loginBlock.style.display = 'flex';
});

from3to2Svg.addEventListener('click', function() {
    regBlock.style.display = 'none';
    step1Block.style.display = 'flex';
});

createAccount.addEventListener('click', function (){

    const fullName = document.querySelector('#name');
    const email = document.querySelector('#email');
    const password = document.querySelector('#password');
    const passwordNext = document.querySelector('#password_next');

    const userInfo = validation(fullName, email, password, passwordNext);
    const type = userType(typeInputs);

    if(type === 'teacher') {

        teacher.type = 'teacher';
        teacher.name = userInfo.name;
        teacher.email = userInfo.email;
        teacher.password = userInfo.password;

        localStorage.setItem('teacher', JSON.stringify(teacher));

        saveToStorage(teacher);

        document.querySelector('#name').value = '';
        document.querySelector('#email').value = '';
        document.querySelector('#password').value = '';
        document.querySelector('#password_next').value = '';

        window.location.href = '/teacher.html'

    } else {

        student.push(
            {
                type : 'student',
                name : userInfo.name,
                email : userInfo.email,
                password : userInfo.password,
            }
        )

        localStorage.setItem('student', JSON.stringify(student));

        saveToStorage({
            type : 'student',
            name : userInfo.name,
            email : userInfo.email,
            password : userInfo.password,
        });

        document.querySelector('#name').value = '';
        document.querySelector('#email').value = '';
        document.querySelector('#password').value = '';
        document.querySelector('#password_next').value = '';

        window.location.href = '/student.html'

    }


});

function userType(inputs) {

    let type = '';

    inputs.forEach((el) => {

        if(el.checked) {

            type = el.id.slice(5);

        }

    });

    return type;

}

function validation(name, email, pass, pass2) {

    if(name.value === ''){

        name.classList.add('error');
        return false;

    } else {

        name.classList.remove('error');

    }

    const firstName = name.value.split(' ')[0];
    const lastName = name.value.split(`${firstName} `)[1];

    if(firstName === undefined || firstName.length <= 1 || lastName === undefined || lastName.length <= 1 ) {

        name.classList.add('error');
        return false;

    } else {

        name.classList.remove('error');

    }

    if(pass.value !== pass2.value){

        pass.classList.add('error');
        pass2.classList.add('error');
        return false;

    } else {

        pass.classList.remove('error');
        pass2.classList.remove('error');

    }

    return {name:name.value, email:email.value, password:pass.value};

}